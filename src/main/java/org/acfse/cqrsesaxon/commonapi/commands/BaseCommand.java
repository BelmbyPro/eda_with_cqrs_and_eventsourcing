package org.acfse.cqrsesaxon.commonapi.commands;

import lombok.Getter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

public abstract class BaseCommand<T> {
    /*
    * l'id de la commande représente toujours l'id de l'aggregat (compte par exple)
    *  sur lequel on effectue la commande
    * l'annotation @TargetAggregateIdentifier permet de dire que l'id de la commande est la
    * même de l'id de l'aggregat (compte par exple)
    *
    * les commandes et les évts sont des objets immuables càd on ne doit pas pouvoir les modifier
     * donc pas besoin de créer des setters ==> les getters seules suiffisent
     * */
    @TargetAggregateIdentifier
    @Getter private T id;

    public BaseCommand(T id) {
        this.id = id;
    }
}
