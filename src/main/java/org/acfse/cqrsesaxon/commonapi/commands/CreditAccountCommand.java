package org.acfse.cqrsesaxon.commonapi.commands;

import lombok.Getter;

public class CreditAccountCommand extends BaseCommand<String> {
    @Getter private double amount;
    @Getter private String currency;

    /*
     * l'id ici est l'identifiant de la classe Account
     * et représente le numéro de compte
     * */

    public CreditAccountCommand(String id, double amount, String currency) {
        super(id);
        this.amount = amount;
        this.currency = currency;
    }
}
