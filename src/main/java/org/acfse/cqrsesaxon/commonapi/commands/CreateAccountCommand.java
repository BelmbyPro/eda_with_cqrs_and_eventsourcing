package org.acfse.cqrsesaxon.commonapi.commands;

import lombok.Getter;

public class CreateAccountCommand extends BaseCommand<String> {
   @Getter private double initialBalance;
   @Getter private String currency;

    /*
     * l'id ici est l'identifiant de la classe Account
     * et représente le numéro de compte
     * */

    public CreateAccountCommand(String id, double initialBalance, String currency) {
        super(id);
        this.initialBalance = initialBalance;
        this.currency = currency;
    }
}
