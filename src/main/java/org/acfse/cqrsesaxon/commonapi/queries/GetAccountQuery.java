package org.acfse.cqrsesaxon.commonapi.queries;

import lombok.Data;

@Data
public class GetAccountQuery {
    private String id;
    public GetAccountQuery(String id) {
        this.id = id;
    }
}
