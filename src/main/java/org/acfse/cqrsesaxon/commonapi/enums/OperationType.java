package org.acfse.cqrsesaxon.commonapi.enums;

public enum OperationType {
    CREDIT, DEBIT
}
