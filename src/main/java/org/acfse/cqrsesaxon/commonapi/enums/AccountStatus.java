package org.acfse.cqrsesaxon.commonapi.enums;

public enum AccountStatus {
    CREATED, ACTIVATED, SUSPENDED
}
