package org.acfse.cqrsesaxon.commands.aggregates;

import org.acfse.cqrsesaxon.commonapi.commands.CreateAccountCommand;
import org.acfse.cqrsesaxon.commonapi.commands.CreditAccountCommand;
import org.acfse.cqrsesaxon.commonapi.commands.DebitAccountCommand;
import org.acfse.cqrsesaxon.commonapi.enums.AccountStatus;
import org.acfse.cqrsesaxon.commonapi.events.AccountActivatedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountCreatedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountCreditedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountDebitedEvent;
import org.acfse.cqrsesaxon.commonapi.exceptions.BalanceNotSufficientException;
import org.acfse.cqrsesaxon.commonapi.exceptions.NegativeAmountException;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class AccountAggregate {
    @AggregateIdentifier
    private String accountId;
    private double balance;
    private String currency;
    private AccountStatus status;

    public AccountAggregate() {
        //required by AXON  because it's used to restitutive aggregate from  event Store
    }

    @CommandHandler // mean that we subscribe on command bus (Fonction de décision)
    public AccountAggregate(CreateAccountCommand createAccountCommand) {

        if (createAccountCommand.getInitialBalance() < 0){
            throw new RuntimeException("impossible...");
        }

        //si tout est ok on crée un event

        AggregateLifecycle.apply(
                new AccountCreatedEvent(
                        createAccountCommand.getId(),
                        createAccountCommand.getInitialBalance(),
                        createAccountCommand.getCurrency(),
                        AccountStatus.CREATED)
        );

        //une fois l'event émis c'est AXON qui se charge de le persisté
    }

    @EventSourcingHandler //(Fonction d'évolution)
    public void on(AccountCreatedEvent event){
        this.accountId = event.getId();
        this.balance = event.getInitialBalance();
        this.currency = event.getCurrency();
        this.status = AccountStatus.CREATED;

        //un event peut nécessiter la création d'un autre

        AggregateLifecycle.apply(
                new AccountActivatedEvent(
                        event.getId(),
                        AccountStatus.ACTIVATED
                )
        );
    }

    @EventSourcingHandler
    public void on(AccountActivatedEvent event){
        this.status = event.getStatus();
    }

    @CommandHandler
    public void handle(CreditAccountCommand command){
        if (command.getAmount() < 0){
            throw new NegativeAmountException("amount should not be negative");
        }

        AggregateLifecycle.apply(
                new AccountCreditedEvent(
                        command.getId(),
                        command.getAmount(),
                        command.getCurrency()
                )
        );
    }

    @EventSourcingHandler
    public void on(AccountCreditedEvent event){
        this.balance += event.getAmount();
    }

    @CommandHandler
    public void handle(DebitAccountCommand command){

        if (command.getAmount() < 0){
            throw new NegativeAmountException("amount should not be negative");
        }
        if (this.balance< command.getAmount()){
            throw new BalanceNotSufficientException("Balance not sufficient");
        }

        AggregateLifecycle.apply(
                new AccountDebitedEvent(
                        command.getId(),
                        command.getAmount(),
                        command.getCurrency()
                )
        );
    }

    @EventSourcingHandler
    public void on(AccountDebitedEvent event){
        this.balance -= event.getAmount();
    }
}
