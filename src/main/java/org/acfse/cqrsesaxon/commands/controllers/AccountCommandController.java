package org.acfse.cqrsesaxon.commands.controllers;

import lombok.AllArgsConstructor;
import org.acfse.cqrsesaxon.commonapi.commands.CreateAccountCommand;
import org.acfse.cqrsesaxon.commonapi.commands.CreditAccountCommand;
import org.acfse.cqrsesaxon.commonapi.commands.DebitAccountCommand;
import org.acfse.cqrsesaxon.commonapi.dtos.AccountRequestDto;
import org.acfse.cqrsesaxon.commonapi.dtos.CreditAccountRequestDto;
import org.acfse.cqrsesaxon.commonapi.dtos.DebitAccountRequestDto;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "/commands/account")
@AllArgsConstructor
public class AccountCommandController {
    /*
    * CommandGateway  Broker qui sera en charge de véhiculer les commandes vers le bus de commande
    * */
    private CommandGateway commandGateway;
    private EventStore eventStore;

    /*
    * CompletableFuture c'est la demande reponse async
    * il veut tout simplement dire que j'ai émis une commande et lorsqu'elle
    * sera traitée vous me renvoyez la réponse*/

    @PostMapping(path = "/create")
    public CompletableFuture<String> createAccount(@RequestBody AccountRequestDto accountRequestDto){
        CreateAccountCommand createAccountCommand = new CreateAccountCommand(
                UUID.randomUUID().toString(),
                accountRequestDto.getInitialBalance(),
                accountRequestDto.getCurrency()
        );
       CompletableFuture<String> commandResponse =  commandGateway.send(createAccountCommand);
        return commandResponse;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> getExeptions(Exception e){
        return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/eventStore/{accountId}")
    public Stream eventStore(@PathVariable String accountId){
        return eventStore.readEvents(accountId).asStream();
    }

    @PutMapping(path = "/credit")
    public CompletableFuture<String> creditAccount(@RequestBody CreditAccountRequestDto accountRequestDto){
        CreditAccountCommand creditAccountCommand = new CreditAccountCommand(
                accountRequestDto.getAccountId(),
                accountRequestDto.getAmount(),
                accountRequestDto.getCurrency()
        );
        CompletableFuture<String> commandResponse =  commandGateway.send(creditAccountCommand);
        return commandResponse;
    }

    @PutMapping(path = "/debit")
    public CompletableFuture<String> debitAccount(@RequestBody DebitAccountRequestDto accountRequestDto){
        DebitAccountCommand command = new DebitAccountCommand(
                accountRequestDto.getAccountId(),
                accountRequestDto.getAmount(),
                accountRequestDto.getCurrency()
        );
        CompletableFuture<String> commandResponse =  commandGateway.send(command);
        return commandResponse;
    }
}
