package org.acfse.cqrsesaxon.query.repositories;

import org.acfse.cqrsesaxon.query.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,String> {
}
