package org.acfse.cqrsesaxon.query.repositories;

import org.acfse.cqrsesaxon.query.entities.Account;
import org.acfse.cqrsesaxon.query.entities.Operation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationRepository extends JpaRepository<Operation,Long> {
}
