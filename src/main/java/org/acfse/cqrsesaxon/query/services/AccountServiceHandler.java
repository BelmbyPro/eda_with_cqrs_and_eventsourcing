package org.acfse.cqrsesaxon.query.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.acfse.cqrsesaxon.commonapi.enums.OperationType;
import org.acfse.cqrsesaxon.commonapi.events.AccountActivatedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountCreatedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountCreditedEvent;
import org.acfse.cqrsesaxon.commonapi.events.AccountDebitedEvent;
import org.acfse.cqrsesaxon.commonapi.queries.GetAccountQuery;
import org.acfse.cqrsesaxon.commonapi.queries.GetAllAccountsQuery;
import org.acfse.cqrsesaxon.query.entities.Account;
import org.acfse.cqrsesaxon.query.entities.Operation;
import org.acfse.cqrsesaxon.query.repositories.AccountRepository;
import org.acfse.cqrsesaxon.query.repositories.OperationRepository;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class AccountServiceHandler {

    private AccountRepository accountRepository;
    private OperationRepository operationRepository;

    @EventHandler
    public void on(AccountCreatedEvent event){

        log.info("*************************");
        log.info("AccountCreatedEvent received");

        Account account = new Account();
        account.setId(event.getId());
        account.setBalance(event.getInitialBalance());
        account.setCurrency(event.getCurrency());
        account.setStatus(event.getStatus());

        accountRepository.save(account);

    }

    @EventHandler
    public void on(AccountActivatedEvent event){

        log.info("*************************");
        log.info("AccountActivatedEvent received");

        Account account = accountRepository.findById(event.getId()).get();
        account.setStatus(event.getStatus());

        /* à défaut de faire un save explicite, on peut juste
         rendre le service transactionnel en rajoutant l'annotation @transactional
         ainsi le save sera fait implicitement*/
        accountRepository.save(account);

    }

    @EventHandler
    public void on(AccountDebitedEvent event){

        log.info("*************************");
        log.info("AccountDebitedEvent received");

        Account account = accountRepository.findById(event.getId()).get();

        //on crée une opération de débit sur le compte
        Operation  operation = new Operation();
        operation.setAmount(event.getAmount());
        operation.setDate(new Date()); // à ne pas faire. Le faire à la création de l'event
        operation.setOperationType(OperationType.DEBIT);
        operation.setAccount(account);

        operationRepository.save(operation);

        /* à défaut de faire un save explicite, on peut juste
         rendre le service transactionnel en rajoutant l'annotation @transactional
         ainsi le save sera fait implicitement*/
        account.setBalance(account.getBalance() - event.getAmount());
        accountRepository.save(account);

    }

    @EventHandler
    public void on(AccountCreditedEvent event){

        log.info("*************************");
        log.info("AccountCreditedEvent received");

        Account account = accountRepository.findById(event.getId()).get();

        //on crée une opération de crédit sur le compte
        Operation  operation = new Operation();
        operation.setAmount(event.getAmount());
        operation.setDate(new Date()); // à ne pas faire. Le faire à la création de l'event
        operation.setOperationType(OperationType.CREDIT);
        operation.setAccount(account);

        operationRepository.save(operation);

        /* à défaut de faire un save explicite, on peut juste
         rendre le service ou la méthode transactionnel en rajoutant l'annotation @transactional
         ainsi le save sera fait implicitement*/
        account.setBalance(account.getBalance() + event.getAmount());
        accountRepository.save(account);

    }

    @QueryHandler
    public List<Account> on(GetAllAccountsQuery query){
        return accountRepository.findAll();
    }

    @QueryHandler
    public Account on(GetAccountQuery query){
       return accountRepository.findById(query.getId()).get();
    }
}
