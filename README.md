# EDA_WITH_CQRS_AND_EVENTSOURCING

POC sur l'EDA (Event Driven Architecture) avec implémentation des patterns d'architectures Stratégique CQRS (Command Query Responsability Segregation) et EventSourcing

Frameworks utilisés

- Spring Boot

- AXON Framework et AXON Server
